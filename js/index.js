//生成菜单
var menuItem = Vue.extend({
	name: 'menu-item',
	props:{item:{},index:0},
	template:[
	          '<li :class="{active: (item.type===0 && index === 0)}">',
				  '<a v-if="item.type === 0" href="javascript:;">',
					  '<i v-if="item.icon != null" :class="item.icon"></i>',
					  '<span>{{item.name}}</span>',
					  '<i class="fa fa-angle-left pull-right"></i>',
				  '</a>',
				  '<ul v-if="item.type === 0" class="treeview-menu">',
					  '<menu-item :item="item" :index="index" v-for="(item, index) in item.list"></menu-item>',
				  '</ul>',
				  '<a v-if="item.type === 1" :href="\'#\'+item.url">' +
					  '<i v-if="item.icon != null" :class="item.icon"></i>' +
					  '<i v-else class="fa fa-circle-o"></i> {{item.name}}' +
				  '</a>',
	          '</li>'
	].join('')
});

//iframe自适应
$(window).on('resize', function() {
	var $content = $('.content');
	$content.height($(this).height() - 120);
	$content.find('iframe').each(function() {
		$(this).height($content.height());
	});
}).resize();

//注册菜单组件
Vue.component('menuItem',menuItem);

var vm = new Vue({
	el:'#rrapp',
	data:{
		user:{},
		menuList:{},
		main:"main.html",
		password:'',
		newPassword:'',
        navTitle:"欢迎页"
	},
	methods: {
		getMenuList: function () {
			this.menuList = [
                {
                    "menuId": 1,
                    "parentId": 0,
                    "parentName": null,
                    "name": "用户管理",
                    "url": null,
                    "perms": null,
                    "type": 0,
                    "icon": "fa fa-users",
                    "orderNum": 0,
                    "open": null,
                    "list": [
                        {
                            "menuId": 2,
                            "parentId": 1,
                            "parentName": null,
                            "name": "管理员管理",
                            "url": "modules/sys/user.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-user",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
						{
                            "menuId": 3,
                            "parentId": 1,
                            "parentName": null,
                            "name": "教练管理",
                            "url": "modules/sys/coach.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-user",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 4,
                            "parentId": 1,
                            "parentName": null,
                            "name": "会员管理",
                            "url": "modules/sys/member.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-user",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        }
                    ]
                },
				{
                    "menuId": 5,
                    "parentId": 0,
                    "parentName": null,
                    "name": "基础设置",
                    "url": null,
                    "perms": null,
                    "type": 0,
                    "icon": "fa fa-cog",
                    "orderNum": 0,
                    "open": null,
                    "list": [
                        {
                            "menuId": 6,
                            "parentId": 5,
                            "parentName": null,
                            "name": "头图管理",
                            "url": "modules/sys/banner.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-picture-o",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 7,
                            "parentId": 5,
                            "parentName": null,
                            "name": "优惠券管理",
                            "url": "modules/sys/coupon.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-money",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 13,
                            "parentId": 5,
                            "parentName": null,
                            "name": "邀请有礼配置",
                            "url": "modules/sys/invitation.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-user",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        }
                    ]
                },
                {
                    "menuId": 8,
                    "parentId": 0,
                    "parentName": null,
                    "name": "课程管理",
                    "url": null,
                    "perms": null,
                    "type": 0,
                    "icon": "fa fa-tasks",
                    "orderNum": 0,
                    "open": null,
                    "list": [
                        {
                            "menuId": 9,
                            "parentId": 8,
                            "parentName": null,
                            "name": "私教课程核销列表",
                            "url": "modules/sys/verification.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-bars",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 10,
                            "parentId": 8,
                            "parentName": null,
                            "name": "团课管理",
                            "url": "modules/sys/grouplesson.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-tags",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 11,
                            "parentId": 8,
                            "parentName": null,
                            "name": "训练营管理",
                            "url": "modules/sys/battalionlesson.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-life-ring",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 9,
                            "parentId": 8,
                            "parentName": null,
                            "name": "课程时间管理",
                            "url": "modules/sys/perlesson.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-tag",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        }
                    ]
                },
                {
                    "menuId": 12,
                    "parentId": 0,
                    "parentName": null,
                    "name": "充值管理",
                    "url": null,
                    "perms": null,
                    "type": 0,
                    "icon": "fa fa-cc-paypal",
                    "orderNum": 0,
                    "open": null,
                    "list": [
                        {
                            "menuId": 13,
                            "parentId": 12,
                            "parentName": null,
                            "name": "充值列表配置",
                            "url": "modules/sys/recharge.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-list",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        },
                        {
                            "menuId": 14,
                            "parentId": 12,
                            "parentName": null,
                            "name": "会员历史账单",
                            "url": "modules/sys/rechargelist.html",
                            "perms": null,
                            "type": 1,
                            "icon": "fa fa-bars",
                            "orderNum": 1,
                            "open": null,
                            "list": null
                        }

                    ]
                }
            ]
		},
		getUser: function(){
			$.getJSON(apiBaseURL + "sys/user/info", function(r){
				vm.user = r.user;
			});
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: jQuery("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: apiBaseURL + "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(r){
							if(r.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(){
									location.reload();
								});
							}else{
								layer.alert(r.msg);
							}
						}
					});
	            }
			});
		},
        logout: function () {
			//删除本地token
            localStorage.removeItem("token");
            //跳转到登录页面
            location.href = baseURL + 'login.html';
        }
	},
	created: function(){
		this.getMenuList();
		this.getUser();
	},
	updated: function(){
		//路由
		var router = new Router();
		routerList(router, vm.menuList);
		router.start();
	}
});



function routerList(router, menuList){
	for(var key in menuList){
		var menu = menuList[key];
		if(menu.type == 0){
			routerList(router, menu.list);
		}else if(menu.type == 1){
			router.add('#'+menu.url, function() {
				var url = window.location.hash;
				//替换iframe的url
			    vm.main = url.replace('#', '');
			    //导航菜单展开
			    $(".treeview-menu li").removeClass("active");
                $(".sidebar-menu li").removeClass("active");
			    $("a[href='"+url+"']").parents("li").addClass("active");

			    vm.navTitle = $("a[href='"+url+"']").text();
			});
		}
	}
}
