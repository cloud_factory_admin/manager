require.config({
    urlArgs: "ver=1.0_0",
    paths:{
        "vue":'../../../libs/vue.min',
        "sortablejs":'Sortable',
        "vuedraggable":'vuedraggable'
    },
    shim:{
        'vue':{
            exports:'vue'
        }
    }
});
require(['vue','vuedraggable'],function(Vue,draggable){
    Vue.component('draggable', draggable);
    console.log("banner init");
    var vm = new Vue({
        el: '#pgbanner',
        data: {
            bannerList:{},
            showList:true,
            title:"",
            banner:{},
            uploadInitAble:true
        },
        methods:{
            getdata: function(evt){
                console.log(evt.draggedContext.element.id);
            },
            addBanner: function () {
                vm.showList = false;
                vm.title = "新增";
            },
            updateBanner:function (id) {
                if(id){
                    vm.showList = false;
                    vm.title = "修改";
                    vm.getBanner(id)
                    //vm.initFileUpload();
                }else {
                    alert("请选择要更新的数据");
                }

            },
            delBanner:function (id) {
                if (id){
                    confirm('确定要删除选中的记录？', function(){
                        $.ajax({
                            type: "DELETE",
                            url: apiBaseURL + "sys/banner/"+id,
                            success: function(r){
                                if(r.code == 0){
                                    alert('操作成功', function(){
                                        vm.getBannerList();
                                    });
                                }else{
                                    alert(r.msg);
                                }
                            }
                        });
                    });
                }
            },
            datadragEnd:function(evt){
                $.ajax({
                    type: "PUT",
                    url: apiBaseURL + "sys/banner/order/"+vm.bannerList[evt.newIndex].bannerId,
                    data: "newIndex="+evt.newIndex,
                    success: function(r){
                        if(r.code === 0){
                            alert('操作成功', function(){
                                vm.getBannerList();
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });

            },
            getBannerList:function () {
                $.getJSON(apiBaseURL + "sys/banner", function(r){
                    vm.bannerList = r.bannerList;
                    vm.showList = true;
                });
            },
            getBanner:function (id) {
                $.getJSON(apiBaseURL + "sys/banner/"+id, function(r){
                    vm.banner = r.banner;
                });
            },
            saveOrUpdate: function () {
                var type = vm.banner.bannerId == null ? "POST" : "PUT";
                $.ajax({
                                type: type,
                                url: apiBaseURL + "sys/banner",
                                contentType: "application/json",
                                data: JSON.stringify(vm.banner),
                                success: function(r){
                                    if(r.code === 0){
                                        alert('操作成功', function(){
                                            vm.banner={};
                                            vm.getBannerList();
                                        });
                                    }else{
                                        alert(r.msg);
                                    }
                    }
                });
            },
            fileUpload: function () {
                var formData = new FormData();
                formData.append('file',$("#file_upload")[0].files[0]);    //将文件转成二进制形式
                $.ajax({
                    type:"post",
                    url:apiBaseURL + 'sys/file/upload/img?token=' + token,
                    async:false,
                    contentType: false,    //这个一定要写
                    processData: false, //这个也一定要写，不然会报错
                    data:formData,
                    dataType:'json',    //返回类型，有json，text，HTML。这里并没有jsonp格式，所以别妄想能用jsonp做跨域了。
                    success:function(data){
                        if(data.code == 0){
                            Vue.set(vm.banner,'bannerPic',data.url);
                        }else{
                            alert(data.msg);
                        }
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown, data){
                        alert(errorThrown);
                    }
                });
    }
        },
        created: function(){
            this.getBannerList() ;
        }
    });
})

