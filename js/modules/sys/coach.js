$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/coach',
        datatype: "json",
        colModel: [			
			{ label: '教练id', name: 'coachId', index: "coach_id", width: 25, key: true },
			{ label: '姓名', name: 'coachName', width: 20 },
			{ label: '类别', name: 'coachType', width: 30,formatter: function(value, options, row){
                return value === 1 ?
                    '<span class="label label-danger">健身教练</span>' :
                    '<span class="label label-success">营养师</span>';
            } },
			{ label: '简介', name: 'coachIntro', width: 90 },
			{ label: '特长', name: 'coachTags', width: 80 },
			{ label: '头像', name: 'coachAvatar', width: 30 ,formatter: function(value, options, row){
                return '<img width="54" height="54" src="'+value+'" />'
            }},
			{ label: '创建时间', name: 'createTime', index: "create_time", width: 60},
            { label: '操作', name: 'coachId', width: 75,formatter: function(value, options, row){
                  var name ="'"+row.coachName+"'";
                  var addLesson = '<a  class="btn btn-sm btn-primary " onclick="vm.createLesson('+value+','+name+')"><i class="fa fa-plus"></i>&nbsp;添加课程</a>';
                  var updateBtn =  '<a  class="btn btn-sm btn-primary" onclick="vm.update('+value+')"><i class="fa fa-pencil-square-o"></i>&nbsp;修改</a>';
                  var delBtn = '<a  class="btn btn-sm btn-primary" onclick="vm.del('+value+')"><i class="fa fa-trash-o"></i>&nbsp;删除</a>';
                return updateBtn+delBtn+addLesson;
            } },
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

const CONFIG = {
    HOUR_TOKENS: ['HH', 'H', 'hh', 'h', 'kk', 'k'],
    MINUTE_TOKENS: ['mm', 'm'],
    SECOND_TOKENS: ['ss', 's'],
    APM_TOKENS: ['A', 'a']
}
var VueTimepicker = Vue.extend({
    name: 'vue-timepicker',
    props: {
        value: {type: Object},
        hideClearButton: {type: Boolean},
        format: {type: String},
        minuteInterval: {type: Number},
        secondInterval: {type: Number}
    },

    data () {
        return {
            hours: [],
            minutes: [],
            seconds: [],
            apms: [],
            showDropdown: false,
            muteWatch: false,
            hourType: 'HH',
            minuteType: 'mm',
            secondType: '',
            apmType: '',
            hour: '',
            minute: '',
            second: '',
            apm: '',
            fullValues: undefined
        }
    },

    computed: {
        displayTime () {
            let formatString = String((this.format || 'HH:mm'))
            if (this.hour) {
                formatString = formatString.replace(new RegExp(this.hourType, 'g'), this.hour)
            }
            if (this.minute) {
                formatString = formatString.replace(new RegExp(this.minuteType, 'g'), this.minute)
            }
            if (this.second && this.secondType) {
                formatString = formatString.replace(new RegExp(this.secondType, 'g'), this.second)
            }
            if (this.apm && this.apmType) {
                formatString = formatString.replace(new RegExp(this.apmType, 'g'), this.apm)
            }
            return formatString
        },
        showClearBtn () {
            if ((this.hour && this.hour !== '') || (this.minute && this.minute !== '')) {
                return true
            }
            return false
        }
    },

    watch: {
        'format': 'renderFormat',
        minuteInterval (newInteval) {
            this.renderList('minute', newInteval)
        },
        secondInterval (newInteval) {
            this.renderList('second', newInteval)
        },
        'value': 'readValues',
        'displayTime': 'fillValues'
    },

    methods: {
        formatValue (type, i) {
            switch (type) {
                case 'H':
                case 'm':
                case 's':
                    return String(i)
                case 'HH':
                case 'mm':
                case 'ss':
                    return i < 10 ? `0${i}` : String(i)
                case 'h':
                case 'k':
                    return String(i + 1)
                case 'hh':
                case 'kk':
                    return (i + 1) < 10 ? `0${i + 1}` : String(i + 1)
                default:
                    return ''
            }
        },

        checkAcceptingType (validValues, formatString, fallbackValue) {
            if (!validValues || !formatString || !formatString.length) { return '' }
            for (let i = 0; i < validValues.length; i++) {
                if (formatString.indexOf(validValues[i]) > -1) {
                    return validValues[i]
                }
            }
            return fallbackValue || ''
        },

        renderFormat (newFormat) {
            newFormat = newFormat || this.format
            if (!newFormat || !newFormat.length) {
                newFormat = 'HH:mm'
            }

            this.hourType = this.checkAcceptingType(CONFIG.HOUR_TOKENS, newFormat, 'HH')
            this.minuteType = this.checkAcceptingType(CONFIG.MINUTE_TOKENS, newFormat, 'mm')
            this.secondType = this.checkAcceptingType(CONFIG.SECOND_TOKENS, newFormat)
            this.apmType = this.checkAcceptingType(CONFIG.APM_TOKENS, newFormat)

            this.renderHoursList()
            this.renderList('minute')

            if (this.secondType) {
                this.renderList('second')
            }

            if (this.apmType) {
                this.renderApmList()
            }

            const self = this
            this.$nextTick(() => {
                self.readValues()
            })
        },

        renderHoursList () {
            const hoursCount = (this.hourType === 'h' || this.hourType === 'hh') ? 12 : 24
            this.hours = []
            for (let i = 0; i < hoursCount; i++) {
                this.hours.push(this.formatValue(this.hourType, i))
            }
        },

        renderList (listType, interval) {
            if (listType === 'second') {
                interval = interval || this.secondInterval
            } else if (listType === 'minute') {
                interval = interval || this.minuteInterval
            } else {
                return
            }

            if (interval === 0) {
                interval = 60
            } else if (interval > 60) {
                window.console.warn('`' + listType + '-interval` should be less than 60. Current value is', interval)
                interval = 1
            } else if (interval < 1) {
                window.console.warn('`' + listType + '-interval` should be NO less than 1. Current value is', interval)
                interval = 1
            } else if (!interval) {
                interval = 1
            }

            if (listType === 'minute') {
                this.minutes = []
            } else {
                this.seconds = []
            }

            for (let i = 0; i < 60; i += interval) {
                if (listType === 'minute') {
                    this.minutes.push(this.formatValue(this.minuteType, i))
                } else {
                    this.seconds.push(this.formatValue(this.secondType, i))
                }
            }
        },

        renderApmList () {
            this.apms = []
            if (!this.apmType) { return }
            this.apms = this.apmType === 'A' ? ['AM', 'PM'] : ['am', 'pm']
        },

        readValues () {
            if (!this.value || this.muteWatch) { return }

            const timeValue = JSON.parse(JSON.stringify(this.value || {}))

            const values = Object.keys(timeValue)
            if (values.length === 0) { return }

            if (values.indexOf(this.hourType) > -1) {
                this.hour = timeValue[this.hourType]
            }

            if (values.indexOf(this.minuteType) > -1) {
                this.minute = timeValue[this.minuteType]
            }

            if (values.indexOf(this.secondType) > -1) {
                this.second = timeValue[this.secondType]
            } else {
                this.second = 0
            }

            if (values.indexOf(this.apmType) > -1) {
                this.apm = timeValue[this.apmType]
            }

            this.fillValues()
        },

        fillValues () {
            let fullValues = {}

            const baseHour = this.hour
            const baseHourType = this.hourType

            const hourValue = baseHour || baseHour === 0 ? Number(baseHour) : ''
            const baseOnTwelveHours = this.isTwelveHours(baseHourType)
            const apmValue = (baseOnTwelveHours && this.apm) ? String(this.apm).toLowerCase() : false

            CONFIG.HOUR_TOKENS.forEach((token) => {
                if (token === baseHourType) {
                    fullValues[token] = baseHour
                    return
                }

                let value
                let apm
                switch (token) {
                    case 'H':
                    case 'HH':
                        if (!String(hourValue).length) {
                            fullValues[token] = ''
                            return
                        } else if (baseOnTwelveHours) {
                            if (apmValue === 'pm') {
                                value = hourValue < 12 ? hourValue + 12 : hourValue
                            } else {
                                value = hourValue % 12
                            }
                        } else {
                            value = hourValue % 24
                        }
                        fullValues[token] = (token === 'HH' && value < 10) ? `0${value}` : String(value)
                        break
                    case 'k':
                    case 'kk':
                        if (!String(hourValue).length) {
                            fullValues[token] = ''
                            return
                        } else if (baseOnTwelveHours) {
                            if (apmValue === 'pm') {
                                value = hourValue < 12 ? hourValue + 12 : hourValue
                            } else {
                                value = hourValue === 12 ? 24 : hourValue
                            }
                        } else {
                            value = hourValue === 0 ? 24 : hourValue
                        }
                        fullValues[token] = (token === 'kk' && value < 10) ? `0${value}` : String(value)
                        break
                    case 'h':
                    case 'hh':
                        if (apmValue) {
                            value = hourValue
                            apm = apmValue || 'am'
                        } else {
                            if (!String(hourValue).length) {
                                fullValues[token] = ''
                                fullValues.a = ''
                                fullValues.A = ''
                                return
                            } else if (hourValue > 11) {
                                apm = 'pm'
                                value = hourValue === 12 ? 12 : hourValue % 12
                            } else {
                                if (baseOnTwelveHours) {
                                    apm = ''
                                } else {
                                    apm = 'am'
                                }
                                value = hourValue % 12 === 0 ? 12 : hourValue
                            }
                        }
                        fullValues[token] = (token === 'hh' && value < 10) ? `0${value}` : String(value)
                        fullValues.a = apm
                        fullValues.A = apm.toUpperCase()
                        break
                }
            })

            if (this.minute || this.minute === 0) {
                const minuteValue = Number(this.minute)
                fullValues.m = String(minuteValue)
                fullValues.mm = minuteValue < 10 ? `0${minuteValue}` : String(minuteValue)
            } else {
                fullValues.m = ''
                fullValues.mm = ''
            }

            if (this.second || this.second === 0) {
                const secondValue = Number(this.second)
                fullValues.s = String(secondValue)
                fullValues.ss = secondValue < 10 ? `0${secondValue}` : String(secondValue)
            } else {
                fullValues.s = ''
                fullValues.ss = ''
            }

            this.fullValues = fullValues
            this.updateTimeValue(fullValues)
            this.$emit('change', {data: fullValues})
        },

        updateTimeValue (fullValues) {
            this.muteWatch = true

            const self = this

            const baseTimeValue = JSON.parse(JSON.stringify(this.value || {}))
            let timeValue = {}

            Object.keys(baseTimeValue).forEach((key) => {
                timeValue[key] = fullValues[key]
            })

            this.$emit('input', timeValue)

            this.$nextTick(() => {
                self.muteWatch = false
            })
        },

        isTwelveHours (token) {
            return token === 'h' || token === 'hh'
        },

        toggleDropdown () {
            this.showDropdown = !this.showDropdown
        },

        select (type, value) {
            if (type === 'hour') {
                this.hour = value
            } else if (type === 'minute') {
                this.minute = value
            } else if (type === 'second') {
                this.second = value
            } else if (type === 'apm') {
                this.apm = value
            }
        },

        clearTime () {
            this.hour = ''
            this.minute = ''
            this.second = ''
            this.apm = ''
        }
    },

    mounted () {
        this.renderFormat()
    },

    template:
    '<span class="time-picker" style="padding: 0px;">' +
    '<input class="display-time"  v-model="displayTime" @click.stop="toggleDropdown" type="text" readonly />' +
    '<span class="clear-btn" v-if="!hideClearButton" v-show="!showDropdown && showClearBtn" @click.stop="clearTime">&times;</span>' +
    '<div class="time-picker-overlay" v-if="showDropdown" @click.stop="toggleDropdown"></div>' +
    '<div class="dropdown" v-show="showDropdown">' +
    '<div class="select-list">' +
    '<ul class="hours">' +
    '<li class="hint" v-text="hourType"></li>' +
    '<li v-for="hr in hours" v-text="hr" :class="{active: hour === hr}" @click.stop="select(\'hour\', hr)"></li>' +
    '</ul>' +
    '<ul class="minutes">' +
    '<li class="hint" v-text="minuteType"></li>' +
    '<li v-for="m in minutes" v-text="m" :class="{active: minute === m}" @click.stop="select(\'minute\', m)"></li>' +
    '</ul>' +
    '<ul class="seconds" v-if="secondType">' +
    '<li class="hint" v-text="secondType"></li>' +
    '<li v-for="s in seconds" v-text="s" :class="{active: second === s}" @click.stop="select(\'second\', s)"></li>' +
    '</ul>' +
    '<ul class="apms" v-if="apmType">' +
    '<li class="hint" v-text="apmType"></li>' +
    '<li v-for="a in apms" v-text="a" :class="{active: apm === a}" @click.stop="select(\'apm\', a)"></li>' +
    '</ul>' +
    '</div>' +
    '</div>' +
    '</span>'
});
Vue.component('vue-timepicker', VueTimepicker);

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            coachName: null,
            coachIntro:null,
            coachTags:null
		},
		showList: true,
		title:null,
		coach:{},
        uploadInitAble:true,
		lessons:{
            coachId:null,
            lessonDate:null,
			items:[]
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.coach = {coachId:null, coachName:null, coachTags:null, coachIntro:null,coachAvatar:null,coachType:null};
		},
		update: function (coachId) {

			vm.showList = false;
            vm.title = "修改";
			vm.getCoach(coachId);
            vm.initFileUpload();
		},
		del: function (coachId) {

			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: apiBaseURL + "sys/coach/delete",
                    contentType: "application/json",
				    data: JSON.stringify(coachId),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var type = vm.coach.coachId == null ? "POST" : "PUT";
			$.ajax({
				type: type,
			    url: apiBaseURL + "sys/coach",
                contentType: "application/json",
			    data: JSON.stringify(vm.coach),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
        getCoach: function(coachId){
			$.get(apiBaseURL + "sys/coach/info/"+coachId, function(r){
				vm.coach = r.coach;
			});
		},
        createLesson: function(coachId,coachName){
            this.lessons.coachId=coachId;
            this.lessons.lessonDate=null;
            this.lessons.items=[];
            layer.open({
                type: 1,
                skin: 'layui-layer-molv',
                title: "添加"+coachName+"的私教课程",
                area: ['800px', '500px'],
                shadeClose: false,
                content: jQuery("#coachLesson"),
                btn: ['保存','取消'],
                btn1: function (index) {
                    var list = vm.lessons.items;
                    for (j=list.length-1;j>=0;j--){
                        if (list[j].lessonBegStr==null||list[j].lessonPrice==null){
                            alert("请先填写空白行");
                            return ;
                        }
                    }
                    $.ajax({
                        type: "POST",
                        contentType: "application/json",
                        url: apiBaseURL + "sys/lesson",
                        data: JSON.stringify(vm.lessons),
                        dataType: "json",
                        success: function(r){
                            if(r.code == 0){
                                layer.close(index);
                                layer.alert('添加成功', function(){
                                    location.reload();
                                });
                            }else{
                                layer.alert(r.msg);
                            }
                        }
                    });
                }
            });
            $('#datepicker').val('');
            $('#datepicker').datetimepicker('remove');
            $('#datepicker').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd',
                startDate:new Date(),
                endDate:'+6d',
                todayBtn:"linked",
                minView:2
            }).on('hide', function(e) {
                var dateStr = $('#datepicker').val();
                if (dateStr){
                    if (vm.lessons.lessonDate!=dateStr){
                        vm.lessons.lessonDate=dateStr;

                        $.get(apiBaseURL + "sys/lesson/coachlesson/"+vm.lessons.coachId+"?begDate="+dateStr+ " 00:00:00", function(r){
                            vm.lessons.items=r.data;
                            if (vm.lessons.items!=null){
                                for(i=0;i<vm.lessons.items.length;i++){
                                    lessonBegStr = vm.lessons.items[i].lessonBegStr;
                                    vm.lessons.items[i].timeData={
                                           HH:lessonBegStr.split(":")[0],
                                            mm:lessonBegStr.split(":")[1]
                                    };
                                }
                            }
                        });
                    }
                }
            });
        },
		reload: function () {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{
                		'coachName': vm.q.coachName,
						'coachIntro':vm.q.coachIntro,
						'coachTags':vm.q.coachTags
						},
                page:page
            }).trigger("reloadGrid");
		},
        fileUpload: function () {
            var formData = new FormData();
            formData.append('file',$("#file_upload")[0].files[0]);    //将文件转成二进制形式
            $.ajax({
                type:"post",
                url:apiBaseURL + 'sys/file/upload/img?token=' + token,
                async:false,
                contentType: false,    //这个一定要写
                processData: false, //这个也一定要写，不然会报错
                data:formData,
                dataType:'json',    //返回类型，有json，text，HTML。这里并没有jsonp格式，所以别妄想能用jsonp做跨域了。
                success:function(data){
                    if(data.code == 0){
                        Vue.set(vm.coach,'coachAvatar',data.url);
                    }else{
                        alert(data.msg);
                    }
                },
                error:function(XMLHttpRequest, textStatus, errorThrown, data){
                    alert(errorThrown);
                }
            });

        },
        addLesson: function () {
            var list = this.lessons.items;
            for (j=list.length-1;j>=0;j--){
                if (list[j].lessonBegStr==null||list[j].lessonPrice==null){
                    alert("请先填写空白行");
                    return ;
                }
            }
            this.sortLesson();
            lessonSize = this.lessons.items.length;
            this.lessons.items.splice(lessonSize,0,{
                lessonBegStr:null,
                lessonPrice:null,
                timeData:{
                    HH:null,
                    mm:null
                },
                flag:1,
                lessonItemId:null
            });
        },
        delLesson: function (index) {
            if ( this.lessons.items[index].lessonItemId==null){
                this.lessons.items.splice(index, 1);
            }else{
                this.lessons.items[index].flag = -1;
                this.lessons.items.splice(index, 0);
            }
        },
        updateLesson: function (index) {
            if (this.lessons.items[index].lessonItemId!=null){
                this.lessons.items[index].flag = 2;
            }
        },
        sortLesson: function () {
            var date = $('#datepicker').val();
            var list = this.lessons.items;
            if (list&&list.length>1){
              list.sort(function (x, y) {//比较函数
                  var start = date+" "+x.lessonBegStr;
                  var end = date+" "+y.lessonBegStr;
                  if (new Date(start) < new Date(end)) {
                      return -1;
                  } else if (new Date(start) > new Date(end)) {
                      return 1;
                  } else {
                      return 0;
                  }
              })
            }
        },
        changeLessonTime:function (eventData,index) {
            var date = $('#datepicker').val();
            value = "";
            if (eventData==null||eventData.data.mm==null||eventData.data.mm==""||eventData.data.HH ==null||eventData.data.HH ==""){
                return ;
            }
            value = eventData.data.HH+":"+eventData.data.mm;
            var list = this.lessons.items;
            if (list[index].lessonBegStr==value){
               return ;
            }
            var curr = date+" "+value;
            this.sortLesson();
            if (list[index].lessonBegStr!=value){
                list[index].lessonBegStr=value;
            }
            this.updateLesson(index)
        }
	}
});
