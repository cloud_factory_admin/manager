$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/couponMaster/list',
        datatype: "json",
        colModel: [
            { label: '券ID', name: 'productNo', width: 45,key:true ,sortable:false},
            { label: '券名称', name: 'name', width: 120 ,sortable:false},
			{ label: '券金额', name: 'marketPrice', width: 30,sortable:false },
			{ label: '券有效开始时间', name: 'validStartTime', width: 90,sortable:false},
			{ label: '券有效结束时间', name: 'validEndTime',  width: 90,sortable:false},
			{ label: '优惠券数量', name: 'productStock', width: 40,sortable:false},
			{ label: '操作', name: 'productNo', width: 90,sortable:false,formatter: function(value, options, row){
                var delBtn = '<a  class="btn btn-danger" onclick="vm.del('+value+')">&nbsp;删除</a>';
                var updateBtn =  '<a  class="btn btn-primary" onclick="vm.update('+value+')"><i class="fa fa-pencil-square-o"></i>&nbsp;查看</a>';
                return updateBtn+delBtn;
            }}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });

    $('#validStartTime').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2,
        minuteStep:1
    }).on('hide', function(e) {
    	if(e.date){
            var dateStr = e.date.Format("yyyy-MM-dd hh:mm:ss");
            if (dateStr){
                vm.q.validStartTime=$('#validStartTime').val();
            }
		}
    });

    $('#validEndTime').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2,
        minuteStep:1
    }).on('hide', function(e) {
        vm.q.validEndTime=$('#validEndTime').val();
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
		    name:null,
            validStartTime:null,
            validEndTime:null
		},
		showList: true,
		addView: false,
        dtlView:false,
        couponMaster:{},
        title:null
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
            $('#add_start').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd',
                todayBtn:"linked",
                minView:2,
                minuteStep:1
            }).on('hide', function(e) {
                if(e.date){
                    if (e.date){
                        var dateStr = e.date.Format("yyyy-MM-dd hh:mm:ss");
                        if (dateStr){
                            vm.couponMaster.validStartTime=dateStr;
                        }
                    }
                }
            });
            $('#add_end').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd',
                todayBtn:"linked",
                minView:2,
                minuteStep:1
            }).on('hide', function(e) {
                if(e.date){
                    if (e.date){
                        var dateStr = e.date.Format("yyyy-MM-dd hh:mm:ss");
                        if (dateStr){
                            vm.couponMaster.validEndTime=dateStr;
                        }
                    }
                }
            });
            vm.couponMaster={}
            vm.addView=true;
		},
		update: function (productNo) {

			vm.showList = false;
            vm.title = "优惠券详情";
			vm.getCouponMaster(productNo);
			vm.dtlView = true;
			this.getCouponItems(productNo);
            $('#dtl_end_time').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd hh:ii:ss',
                todayBtn:"linked",
                minView:0,
                minuteStep:1,
                initialDate:vm.couponMaster.validEndTime
            }).on('hide', function(e) {
                    vm.couponMaster.validEndTime=$('#dtl_end_time').val();
            });
        },
		del: function (productNo) {

			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: apiBaseURL + "sys/couponMaster/"+productNo,
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var url = vm.couponMaster.productNo == null ? "sys/couponMaster/save" : "sys/couponMaster/update";
			$.ajax({
				type: "POST",
			    url: apiBaseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.couponMaster),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		getCouponMaster: function(productNo){
			$.get(apiBaseURL + "sys/couponMaster/info/"+productNo, function(r){
				vm.couponMaster = r.couponMaster;
			});
		},
		reload: function () {
			vm.showList = true;
			vm.addView = false;
			vm.dtlView=false;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{
                        'name':vm.q.name,
                        'validStartTime':vm.q.validStartTime,
                        'validEndTime':vm.q.validEndTime
                },
                page:page
            }).trigger("reloadGrid");
		},
        getCouponItems:function (productNo) {
            $("#product_grid").jqGrid({
                url: apiBaseURL + 'sys/couponMaster/'+productNo+'/items',
                datatype: "json",
                colModel: [
                    { label: '会员id', name: 'memberId', width: 150,sortable:false},
                    { label: '券名称', name: 'name', width: 350 ,sortable:false},
                    { label: '领取时间', name: 'createdTime', width: 180 ,sortable:false},
                    { label: '失效时间', name: 'validEndTime',  width: 180,sortable:false},
                    { label: '优惠券金额', name: 'marketPrice', width: 90,sortable:false},
                    { label: '优惠券状态', name: 'couponStatus', width: 90,sortable:false,formatter: function(value, options, row){
                       if (1==value){
                           return '<span class="label label-success">有效</span>'
                       }else if (2==value){
                           return '<span class="label label-info">已使用</span>'
                       }else if (3==value){
                           return '<span class="label label-default">已过期</span>'
                       }
                        return value;
                    }}
                ],
                viewrecords: true,
                height: 385,
                rowNum: 10,
                rowList : [10,30,50],
                rownumbers: true,
                rownumWidth: 25,
                autowidth:true,
                multiselect: true,
                pager: "#product_grid_pager",
                jsonReader : {
                    root: "page.list",
                    page: "page.currPage",
                    total: "page.totalPage",
                    records: "page.totalCount"
                },
                prmNames : {
                    page:"page",
                    rows:"limit",
                    order: "order"
                },
                gridComplete:function(){
                    //隐藏grid底部滚动条
                    $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
                }
            });
        }
	}
});

