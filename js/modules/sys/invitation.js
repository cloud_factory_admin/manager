$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/promotion/invitation',
        datatype: "json",
        colModel: [
            { label: '活动ID', name: 'promotionId' ,width: 120,sortable:false},
            { label: '活动名称', name: 'promotionName' ,width: 200,sortable:false},
            { label: '活动描述', name: 'promotionDesc' ,width: 250,sortable:false},
            { label: '活动开始时间', name: 'promotionStartTime' ,width: 100,sortable:false},
            { label: '活动结束时间', name: 'promotionEndTime' ,width: 100,sortable:false},
            { label: '是否启用', name: 'promotionStatus',sortable:false ,width: 60, formatter: function(value, options, row){
                return value==1?'<span class="label label-success">启用</span>':'<span class="label label-default">禁用</span>';
            }
            },
            { label: '操作', name: 'promotionStatus', index: "updated_time", width: 90,sortable:false,
                formatter: function(value, options, row){
                    var id = row.promotionId;
                    var btnName ='';
                    var status = 0;
                    if(row.promotionStatus==1){
                        btnName = "禁用";
                        status = 2;
                        return '<a  class="btn btn-sm btn-primary " onclick="vm.del('+id+','+status+')">&nbsp;'+btnName+'</a>';
                    }else if (row.promotionStatus==2){
                        btnName = "启用";
                        status = 1;
                        return '<a  class="btn btn-sm btn-primary " onclick="vm.del('+id+','+status+')">&nbsp;'+btnName+'</a>'+'<a  class="btn btn-sm btn-primary " onclick="vm.update('+id+')">&nbsp;编辑</a>';
                    }
                    return recharge;
                }}
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });

});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            promotionName:null,
            promotionId:null
        },
		showList: true,
		title:null,
        promotion:{
             promotionId:null,
             promotionName:null,
             promotionType:1,
             promotionDesc:null,
             promotionStartTime:null,
             promotionEndTime:null,
             promotionStatus:2,
             coupon:{
                 productNo:null
             }
		},
        coupons:null
	},
	methods: {
		query: function () {
			vm.reload();
		},
        add: function(){
           this.init('新增');
        },
        update:function(id){
            this.getPromotion(id);
            this.init('修改');

        },
        del: function (id,status) {
            confirm('确定要更改状态？', function(){
                $.ajax({
                    type: "POST",
                    url: apiBaseURL + "sys/promotion/invitation/"+id,
                    data: "status="+status,
                    success: function(r){
                        if(r.code == 0){
                            alert('操作成功', function(){
                                vm.reload();
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        saveOrUpdate: function () {
            var url = vm.promotion.promotionId==null?"sys/promotion/invitation/save":"sys/promotion/invitation/update";
            console.log(JSON.stringify(vm.promotion));
            $.ajax({
                type: "POST",
                url: apiBaseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.promotion),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(){
                            vm.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        getPromotion: function(id){
            $.get(apiBaseURL + "sys/promotion/invitation/"+id, function(r){
                vm.promotion = r.promotion;
            });
        },
         getCoupons: function(memberId){
            $.get(apiBaseURL + "sys/couponMaster/listEnable", function(r){
                vm.coupons = r.data;
            });
        },
		reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{
                    'promotionName':vm.q.promotionName,
                    'promotionId':vm.q.promotionId
                },
                page:page
            }).trigger("reloadGrid");
		},
        init:function (title) {
            vm.showList = false;
            vm.title = title;
            this.getCoupons();
            $('#promotionStartTime').val('');
            $('#promotionStartTime').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd',
                startDate:new Date(),
                todayBtn:"linked",
                minView:2
            }).on('hide', function(e) {
                if (vm.promotion.promotionEndTime!=null){
                    if (new Date(vm.promotion.promotionStartTime)>new Date($('#promotionEndTime').val())){
                        alert("开始时间不能大于结束时间");
                        $('#promotionStartTime').val('');
                        return ;
                    }
                }
                vm.promotion.promotionStartTime = $('#promotionStartTime').val();
            });
            $('#promotionEndTime').val('');
            $('#promotionEndTime').datetimepicker({
                autoclose: true,
                language: 'zh-CN',
                format:'yyyy-MM-dd',
                startDate:new Date(),
                todayBtn:"linked",
                minView:2
            }).on('hide', function(e) {
                if ($('#promotionEndTime').val()){
                    if (vm.promotion.promotionStartTime==null||vm.promotion.promotionStartTime==''){
                        alert("请先选择开始时间");
                        $('#promotionEndTime').val()
                        return;
                    }
                    if (new Date(vm.promotion.promotionStartTime)>new Date($('#promotionEndTime').val())){
                        alert("开始时间不能大于结束时间");
                        $('#promotionEndTime').val('')
                        return ;
                    }
                }
                vm.promotion.promotionEndTime = $('#promotionEndTime').val();
            });
        }

	}
});
