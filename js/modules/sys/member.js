$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/member/list',
        datatype: "json",
        colModel: [
            { label: '会员id', name: 'memberId', index: "pm.member_id", width: 45,key:true },
            { label: '手机号', name: 'mobile', index: "mobile", width: 45 },
			{ label: '账户金额', name: 'account.accountAmt', width: 75,sortable:false },
			{ label: '注册时间', name: 'registeredTime', index: "registered_time", width: 90},
			{ label: '修改时间', name: 'updatedTime', index: "updated_time", width: 90,sortable:false},
			{ label: '账户操作', name: 'memberId', index: "updated_time", width: 90,sortable:false,
				formatter: function(value, options, row){
                    var memberId ="'"+row.memberId+"'";
                    var mobile ="'"+row.mobile+"'";
                    var recharge = '<a  class="btn btn-primary" onclick="vm.membereExchange('+memberId+','+mobile+')">&nbsp;充值</a>';
                    var updateBtn =  '<a  class="btn btn-primary" onclick="vm.update('+value+')"><i class="fa fa-pencil-square-o"></i>&nbsp;查看</a>';
                    return updateBtn+recharge;
            }}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });

    $('#registeredBeg').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
        vm.q.registeredBeg=$("#registeredBeg").val();
    });

    $('#registeredEnd').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
        vm.q.registeredEnd=$("#registeredEnd").val();
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			mobile:null,
            registeredBeg:null,
            registeredEnd:null,
            amtLess:null,
			amtMore:null
		},
        lessonBookFlag:{
		    "1":"预约中",
            "2":"已预约",
            "3":"已核销",
            "4":"已取消"
        },
		member:{
		    mobile:null,
            profile:{
					realName:null,
					gender:null
        		    },
            account:{
					accountAmt:null
				    },
            bookedLessons:[],
            rechargeLogs:[],
            couponItems:[]
        },
		showList: true,
		showAdd: false,
		showdtl: false,
		title:null,
		exchange:{memberId:null, mobile:null,exchangeAmt:null}

	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.showAdd = true;
			vm.showdtl = false;
			vm.title = "新增";
			vm.member = {mobile:null, profile:{
                realName:null,
                gender:null
			}};
		},
		update: function (memberId) {

			vm.showList = false;
			vm.showAdd = false;
			vm.showdtl = true;
            vm.title = "会员详情";
			vm.getMember(memberId);
		},
		del: function (memberId) {

			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: apiBaseURL + "sys/member/delete",
                    contentType: "application/json",
				    data: JSON.stringify(memberId),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var url = vm.member.memberId == null ? "sys/member/save" : "sys/member/update";
			$.ajax({
				type: "POST",
			    url: apiBaseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.member),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		getMember: function(memberId){
			$.get(apiBaseURL + "sys/member/info/"+memberId, function(r){
				vm.member = r.member;
			});
		},
		reload: function () {
			vm.showList = true;
			vm.showdtl = false;
			vm.showAdd = false;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'mobile':vm.q.mobile,
                    'registeredBeg':vm.q.registeredBeg,
                    'registeredEnd':vm.q.registeredEnd,
                    'amtLess':vm.q.amtLess,
                    'amtMore':vm.q.amtMore},
                page:page
            }).trigger("reloadGrid");
		},
        membereExchange:function (memberId,mobile) {
			this.exchange.memberId=memberId;
            this.exchange.mobile=mobile;
            vm.exchange.exchangeAmt=null;
            layer.open({
                type: 1,
                skin: 'layui-layer-molv',
                title: "充值",
                area: ['580px', '240px'],
                shadeClose: false,
                content: jQuery("#exchangeLayer"),
                btn: ['充值','关闭'],
                btn1: function (index) {
                    if (vm.exchange.exchangeAmt==null||vm.exchange.exchangeAmt==''||isNaN(vm.exchange.exchangeAmt)){
                    	alert("未填写金额或包含非数字字符");
                    	return ;
					}
					if (vm.exchange.exchangeAmt<0){
                        alert("充值金额需大于0")
                    }
                      $.ajax({
                            type: "POST",
                            url: apiBaseURL + "sys/member/recharge/"+vm.exchange.memberId,
                            data: "exchangeAmt="+vm.exchange.exchangeAmt+"&mobile="+vm.exchange.mobile,
                            success: function(r){
                                if(r.code == 0){
                                    layer.close(index);
                                    layer.alert('修改成功', function(){
                                        location.reload();
                                    });
                                }else{
                                    layer.alert(r.msg);
                                }
                            }
                        });
                }
            });
        }
	}
});