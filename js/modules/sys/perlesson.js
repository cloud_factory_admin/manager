$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/lesson/list',
        datatype: "json",
        colModel: [
            { label: '课程ID', name: 'lessonItemId' ,width:60,sortable:false,key:true},
            { label: '教练', name: 'coachs' ,width:180,sortable:false,formatter: function(value, options, row){
                coachs  = row.coachs;
                coachView = "";
                for (i=0;i<coachs.length;i++){
                    coachView += '<span class="label label-info">'+coachs[i].coachName+'</span>' ;
                }
                return coachView;
            }},
            { label: '课程价格', name: 'lessonPrice' ,sortable:false,width:80},
            { label: '类型', name: 'lessonType',width:60 ,sortable:false,formatter: function(value, options, row){
                 view = "";
                 if (value==1){
                     view = '<span class="label label-info">私教</span>';
                 }else if (value==2){
                     view = '<span class="label label-info">团课</span>';
                 }else if(value==3) {
                     view = '<span class="label label-info">训练营</span>';
                 }
                 return view;
            }},
            { label: '总人数/已预订', name: 'lessonMaxNums',width:100 ,sortable:false,formatter: function(value, options, row){
                return value +"/"+row.lessonBookNums;
            }},
            { label: '开始时间', name: 'lessonBegTime',sortable:false },
            { label: '结束时间', name: 'lessonEndTime',sortable:false },
            { label: '添加时间', name: 'createdTime',sortable:false },
            { label: '操作', name: 'lessonItemId', width: 180,sortable:false,
                formatter: function(value, options, row){
                    //var updateBtn =  '<a  class="btn btn-primary" onclick="vm.update('+value+')"><i class="fa fa-pencil-square-o"></i>&nbsp;查看</a>';
                    var delBtn =  '<a  class="btn btn-primary" onclick="vm.del('+value+')"><i class="fa fa-pencil-square-o"></i>&nbsp;删除</a>';
                    if (row.lessonBookNums&&row.lessonBookNums>0){
                        delBtn = '';
                    }
                    return delBtn;
                }}
        ],
        viewrecords: true,
        height: 400,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
    $('#lessonBeg').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
          vm.q.lessonBeg=$('#lessonBeg').val();
    });
    $('#lessonEnd').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
       vm.q.lessonEnd=$('#lessonEnd').val();
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            lessonBeg:null,
			lessonEnd:null,
			coachId:null,
            lessonStatus:null,
            lessonType:null
		},
		showList: true,
		title:null,
        lesson:{}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
		},
		update: function (lessonId) {
			vm.showList = false;
            vm.title = "修改";
            this.getLesson(lessonId);
		},
		del: function (lessonId) {

			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: apiBaseURL + "sys/lesson/"+lessonId,
                    contentType: "application/json",
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var type = vm.coach.coachId == null ? "POST" : "PUT";
			$.ajax({
				type: type,
			    url: apiBaseURL + "sys/lesson",
                contentType: "application/json",
			    data: JSON.stringify(vm.coach),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
        getLesson: function(lessonId){
			$.get(apiBaseURL + "sys/lesson/info/"+lessonId, function(r){
				vm.lesson = r.lesson;
			});
		},
		reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{
                    'lessonBeg':vm.q.lessonBeg,
                    'lessonEnd':vm.q.lessonEnd,
                    'coachId':vm.q.coachId,
                    'lessonStatus':vm.q.lessonStatus,
                    'lessonType':vm.q.lessonType,
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});