$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/recharge/configs',
        datatype: "json",
        colModel: [
            { label: '记录ID', name: 'rechargeId' ,sortable:false},
            { label: '充值金额', name: 'rechargeAmt' ,sortable:false},
            { label: '赠送金额', name: 'rechargeDiscount' ,sortable:false},
            { label: '创建时间', name: 'createdTime' ,sortable:false},
            { label: '更新时间', name: 'updatedTime' ,sortable:false},
            { label: '是否启用', name: 'rechargeStatus',sortable:false , formatter: function(value, options, row){
                return value==1?'<span class="label label-info">启用</span>':'<span class="label label-default">禁用</span>';
            }
            },
            { label: '操作', name: 'rechargeStatus', index: "updated_time", width: 90,sortable:false,
                formatter: function(value, options, row){
                    var id = row.rechargeId;
                    var btnName ='';
                    var status = 0;
                    if(row.rechargeStatus==1){
                        btnName = "禁用";
                        status = 2;
                    }else if (row.rechargeStatus==2){
                        btnName = "启用";
                        status = 1;
                    }
                    var recharge = '<a  class="btn btn-primary" onclick="vm.del('+id+','+status+')">&nbsp;'+btnName+'</a>';
                    return recharge;
                }}
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });

});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            rechargeAmtMin:null,
            rechargeAmtMax:null,
            rechargeStatus:null
		},
		showList: true,
		title:null,
        recharge:{
            rechargeId:null,
            rechargeAmt:null,
            rechargeDiscount:null,
            rechargeStatus:null
        }

	},
	methods: {
		query: function () {
			vm.reload();
		},
        add: function(){
            vm.showList = false;
            vm.title = "新增";
            vm.recharge={};
        },
        del: function (id,status) {
            confirm('确定要更改状态？', function(){
                $.ajax({
                    type: "POST",
                    url: apiBaseURL + "sys/recharge/config/"+id,
                    data: "status="+status,
                    success: function(r){
                        if(r.code == 0){
                            alert('操作成功', function(){
                                vm.reload();
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        saveOrUpdate: function () {
            var url = "sys/recharge/config";
            $.ajax({
                type: "POST",
                url: apiBaseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.recharge),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(){
                            vm.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        getMember: function(memberId){
            $.get(apiBaseURL + "sys/member/info/"+memberId, function(r){
                vm.member = r.member;
            });
        },
		reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{
                    'rechargeAmtMin':vm.q.rechargeAmtMin,
                    'rechargeAmtMax':vm.q.rechargeAmtMax,
                    'rechargeStatus':vm.q.rechargeStatus
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});
