$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/recharge/list',
        datatype: "json",
        colModel: [
            { label: '会员ID', name: 'memberId' ,sortable:false},
            { label: '会员手机号', name: 'mobile' ,sortable:false},
            { label: '账单类型', name: 'tag' ,sortable:false},
            { label: '账单说明', name: 'content' ,sortable:false},
            { label: '金额', name: 'rechargeAmt' ,sortable:false},
            { label: '课程教练', name: 'coachs' ,sortable:false},
            { label: '账单备注', name: 'note',sortable:false },
            { label: '账单时间', name: 'createdTime',sortable:false }
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
    $('#rechargeBeg').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
          vm.q.rechargeBeg=$('#rechargeBeg').val();
    });
    $('#rechargeEnd').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
       vm.q.rechargeEnd=$('#rechargeEnd').val();
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            rechargeBeg:null,
            rechargeEnd:null,
			mobile:null,
            memberId:null
		},
		showList: true,
		title:null,
	},
	methods: {
		query: function () {
			vm.reload();
		},
		reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{
                    'rechargeBeg':vm.q.rechargeBeg,
                    'rechargeEnd':vm.q.rechargeEnd,
                    'mobile':vm.q.mobile,
                    'memberId':vm.q.memberId
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});