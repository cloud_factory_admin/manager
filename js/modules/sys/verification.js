$(function () {
    $("#jqGrid").jqGrid({
        url: apiBaseURL + 'sys/lesson/verification',
        datatype: "json",
        colModel: [
            { label: '课程编号', name: 'lessonItemId' ,sortable:false},
            { label: '课程开始时间', name: 'lessonBegTime' ,sortable:false},
            { label: '课程结束时间', name: 'lessonEndTime' ,sortable:false},
            { label: '核销教练ID', name: 'coachId' ,sortable:false},
            { label: '核销教练姓名', name: 'coachName',sortable:false },
            { label: '会员ID', name: 'memberId',sortable:false },
            { label: '核销时间', name: 'createdTime',sortable:false }
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
    $('#lessonBeg').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
          vm.q.lessonBeg=$('#lessonBeg').val();
    });
    $('#lessonEnd').datetimepicker({
        autoclose: true,
        language: 'zh-CN',
        format:'yyyy-MM-dd',
        todayBtn:"linked",
        minView:2
    }).on('hide', function(e) {
       vm.q.lessonEnd=$('#lessonEnd').val();
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
            lessonBeg:null,
			lessonEnd:null,
			coachId:null,
            coachName:null
		},
		showList: true,
		title:null,
	},
	methods: {
		query: function () {
			vm.reload();
		},
		reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{
                    'lessonBeg':vm.q.lessonBeg,
                    'lessonEnd':vm.q.lessonEnd,
                    'coachId':vm.q.coachId,
                    'coachName':vm.q.coachName
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});