/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    config.uiColor = '#ECF0F5';

    config.filebrowserImageUploadUrl = apiBaseURL + 'sys/file/upload/ckeditor?token=' + token;//配置图片的上传服务端地址
    config.extraPlugins= 'uploadimage,image2';
    config.stylesSet=[
        { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
        { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
    ];
    config.image_previewText='';
    config.image2_alignClasses= [ 'image-align-left', 'image-align-center', 'image-align-right' ];
    config.image2_disableResizer= true;
    config.removePlugins ="bbcode,elementspath";//配置要移除的插件

    //配置字体
    config.font_names = 'Arial;Times New Roman;Verdana;SimHei;SimSun;NSimSun;FangSong;KaiTi;Microsoft YaHei';
    config.smiley_columns = 16;
    config.wordcount = {
        //显示段落
        showParagraphs: true,
        //单词数量
        showWordCount: true,
        //显示字符数量
        showCharCount: true,
        //将空白作为字符计算
        countSpacesAsChars: true,
        //计算HTML
        countHTML: false,
        //计算换行符
        countLineBreaks: true,
        //最大单词数
        maxWordCount: 8000,
        //最大字符数
        maxCharCount: 8000,
        pasteWarningDuration: 0,
        // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
        filter: new CKEDITOR.htmlParser.filter({
            elements: {
                div: function( element ) {
                    if(element.attributes.class == 'mediaembed') {
                        return false;
                    }
                }
            }
        })
    };
    config.toolbarCanCollapse = true;
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document' ] },
        { name: 'insert', groups: [ 'insert' ] },
        //{ name: 'clipboard', groups: [  'undo' ] },
        //{ name: 'editing', groups: [  'selection', 'spellchecker', 'editing' ] },
        //{ name: 'forms', groups: [ 'forms' ] },
        '/',
       // { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },

        '/',
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];
    config.enterMode = CKEDITOR.ENTER_DIV;
    config.removeButtons = 'Language,ShowBlocks,About,find';
    config.skin="bootstrapck";
};
